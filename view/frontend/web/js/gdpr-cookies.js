/*
 * @author Aleš Cankar
 * @copyright Copyright (c) 2018 Optiweb (https://www.optiweb.com)
 */

define([
    'uiComponent',
], function(Component){
    'use strict';
    var categories;
    return Component.extend({

        isLoading : true,

        initialize: function (config) {
            this._super();
            categories = config.cookieCategory;
            this.isLoading = false;
        },

        getCookies: function () {
            var pairs = document.cookie.split(";"),
                cookies = {
                    'analytics': [],
                    'marketing': [],
                    'mandatory': []
                };
            for (var i = 0; i < pairs.length; i++) {
                var pair = pairs[i].split("="),
                    key = (pair[0] + '').trim(),
                    value = unescape(pair[1]),
                    cat = this.getCookieCategory(key);
                cookies[cat].push({
                    key: key,
                    value: value,
                });
            }
            return cookies;
        },

        getCookieCategory: function (key) {
            if (categories.analytics.indexOf(key) >= 0) {
                return 'analytics';
            } else if (categories.marketing.indexOf(key) >= 0) {
                return 'marketing';
            }
            return 'mandatory';
        },

    });

});