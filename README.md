#Magento 2 - GDPR cookie module
This extension allows users to see which cookies are used, what they do and allows them to disable certain cookies and tracking/marketing scripts from being executed.

---
##Features:
- Display a cookie settings popup on homepage after first visit
- Allow users to disable certain types of cookies (analytics and marketing cookies)
- A page where users can see which cookies the site collects and what they do
- Display user's current cookies and their values

##Compatibility
Developed on 2.2.4, to be tested on other versions.

##Known issues
- Storeowners _must_ implement their tracking scripts in the module configuration, otherwise the module cannot prevent them from being loaded

##Installation:
0. install module: `composer require optiweb/module-gdpr-m2`
10. enable module: `php bin/magento module:enable Optiweb_Gdpr`
20. run `php bin/magento setup:upgrade` from the root directory of your Magento installation

##Changelog
- `1.0.0` initial release
- `1.0.1` code cleanup

##Licence:
MIT. (see `LICENCE.txt`) 