<?php
/**
 * @author Aleš Cankar
 * @copyright Copyright (c) 2018 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\Gdpr\Model\Config\Source;

class EnforceMode
{
    const MANDATORY_COOKIES_ONLY = 1;

    const ALL_COOKIES = 2;

    /**
     * @return array
     * @deprecated
     * To be implemented in later versions
     */
    public function toOptionArray()
    {
        return [
            ['label' => 'Only mandatory cookies', 'value' => self::MANDATORY_COOKIES_ONLY],
            ['label' => 'All cookies', 'value' => self::ALL_COOKIES],
        ];
    }
}