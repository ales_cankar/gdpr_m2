<?php
/**
 * @author Aleš Cankar
 * @copyright Copyright (c) 2018 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\Gdpr\Block;

use \Magento\Store\Model\ScopeInterface;
use \Optiweb\Gdpr\Model\Config\Source\EnforceMode as Enforce;
use \Magento\Framework\Exception\NoSuchEntityException;

class Index extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Cms\Block\Block
     */
    protected $_cmsBlock;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\Module\Dir\Reader
     */
    protected $_reader;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $_request;

    /**
     * @var \Magento\Framework\File\Csv
     */
    protected $_csvReader;

    private $headerScripts = null;

    private $footerScripts = null;

    const TOP_CONTENT_BLOCK_ID = 'cookie_policy__top';

    const POPUP_CONTENT_BLOCK_ID = 'cookie_policy__popup';

    const COOKIE_KEY = 'gdpr_popup_shown';

    const CSV_FILENAME = 'cookies.csv';

    /**
     * Index constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Cms\Model\BlockRepository $_cmsBlock
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $_scopeConfig
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Magento\Framework\Module\Dir\Reader $reader
     * @param \Magento\Framework\File\Csv $csv
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Cms\Model\BlockRepository $_cmsBlock,
        \Magento\Framework\App\Config\ScopeConfigInterface $_scopeConfig,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Module\Dir\Reader $reader,
        \Magento\Framework\File\Csv $csv
    ) {
        $this->_cmsBlock = $_cmsBlock;
        $this->_scopeConfig = $_scopeConfig;
        $this->_request = $request;
        $this->_reader = $reader;
        $this->_csvReader = $csv;
        parent::__construct($context);
    }

    /**
     * @return bool|\Magento\Framework\Phrase|string
     */
    public function getTopContent()
    {
        try {
            $block = $this->_cmsBlock->getById(self::TOP_CONTENT_BLOCK_ID);
        } catch (NoSuchEntityException $e) {
            return __("Top content not configured. Create a CMS block with id '%1'", self::TOP_CONTENT_BLOCK_ID);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        if ($content = $block->getContent()) {
            return $content;
        }
        return false;
    }

    /**
     * @return string|null
     */
    public function getHeaderScripts()
    {
        if ($this->headerScripts === null) {
            $this->headerScripts = $this->_scopeConfig->getValue(
                'ow_gdpr/scripts/header_scripts', ScopeInterface::SCOPE_STORE
            );
        }
        return $this->headerScripts;
    }

    /**
     * @return string|null
     */
    public function getFooterScripts()
    {
        if ($this->footerScripts === null) {
            $this->footerScripts = $this->_scopeConfig->getValue(
                'ow_gdpr/scripts/footer_scripts', ScopeInterface::SCOPE_STORE
            );
        }
        return $this->footerScripts;
    }

    /**
     * @return bool|\Magento\Framework\Phrase|string
     */
    public function getPopupContent()
    {
        try {
            $block = $this->_cmsBlock->getById(self::POPUP_CONTENT_BLOCK_ID);
        } catch (NoSuchEntityException $e) {
            return __("Popup content not configured. Create a CMS block with id '%1'", self::POPUP_CONTENT_BLOCK_ID);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        if ($content = $block->getContent()) {
            return $content;
        }
        return false;
    }

    /**
     * @param string $path
     * @param string $scope
     * @return mixed|null
     */
    public function getConfig($path = '', $scope = ScopeInterface::SCOPE_STORE)
    {
        if (!$path) {
            return null;
        }
        return $this->_scopeConfig->getValue($path, $scope);
    }

    /**
     * @return string
     */
    public function getCookieCategories()
    {
        return json_encode([
            'analytics' => explode(",", $this->getConfig('ow_gdpr/cookies/list_analytics')),
            'marketing' => explode(",", $this->getConfig('ow_gdpr/cookies/list_marketing' ))
        ]);
    }

    /**
     * @return string
     */
    public function isHomePage()
    {
        if ($this->_request->getFullActionName() === 'cms_index_index') {
            return "true";
        }
        return "false";
    }

    /**
     * @return array|bool
     * @throws \Exception
     */
    public function getCSVCookies()
    {
        $file = $this->_reader->getModuleDir('etc', 'Optiweb_Gdpr') . "/" . self::CSV_FILENAME;
        if (!file_exists($file)) {
            return false;
        }
        return $this->_csvReader->getData($file);
    }

    /**
     * @return string|null
     */
    public function getPageTitle()
    {
        return $this->getConfig('ow_gdpr/general/page_title');
    }

}